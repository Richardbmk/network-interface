locals {
  assetname = "rick"
  environment = "example"
  location = "westus"

  resource_name = format("%s-%s-%s", local.assetname, local.environment, local.location)
}

resource "azurerm_resource_group" "resourcegroup" {
  name = "${local.resource_name}-rg-1"
  location = local.location
}

module "network_interface" {
  source = "git::https://gitlab.com/Richardbmk/network-interface.git?ref=v1.0.0"

  instance_count          = 1
  vmname                  = "mynic-${local.environment}"
  resource_group_location = azurerm_resource_group.resourcegroup.location
  resource_group_name     = azurerm_resource_group.resourcegroup.name
  subnet_id               = module.virtual-network.subnet_id[0]
}
# main.tf file of network-interface module
resource "azurerm_network_interface" "nic" {
  count = var.instance_count
  name  = "${var.vmname}-${count.index}"

  location            = var.resource_group_location
  resource_group_name = var.resource_group_name

  ip_configuration {
    name                          = "ipconfiguration-${count.index}"
    subnet_id                     = var.subnet_id
    private_ip_address_allocation = "Dynamic"
  }
}
